//
//  jtb.hpp
//  butt
//
//  Created by jtb on 10/29/18.
//  Copyright © 2018 JT Bullitt. All rights reserved.
//

/*
Some handy utilities.
*/

#ifndef jtb_hpp
#define jtb_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sys/stat.h> // stat()

namespace jtb {
    bool FileExists(const std::string &);
    bool FileExists(char *fileName);
	void ByeBye();
	void ByeBye(int);
    bool path_is_dir(const std::string &);
}

#endif /* jtb_hpp */
