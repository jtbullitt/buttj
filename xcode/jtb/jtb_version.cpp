//
//  jtb_version.cpp
//  butt
//
//  Created by jtb on 2/21/19.
//  Copyright © 2019 JT Bullitt. All rights reserved.
//

#include "jtb_version.hpp"
#include <stdio.h>
#include <iostream>
#include <string>
#include <ctime>
/*
 BuildVersion() -- constructs and returns a plausible version number for this build
 */
std::string BuildVersion() {
	
	// get the XCode build date/time
	char tmp[128];
	sprintf(tmp,"%s%s",__DATE__,__TIME__);
	
	// parse it into a tm struct
	struct tm tm;
	strptime(tmp, "%b %d %Y%H:%M:%S", &tm);
	
	// parse the tm struct back into a friendly date/time string for the "patch" number
	char patch[128];
	strftime(patch, sizeof(patch), VERSION_PATCH_FORMAT, &tm);
	
	char version[128];
	sprintf(version,"%s %d.%03d.%s", VERSION_PROGRAM_NAME, VERSION_MAJOR, VERSION_MINOR, patch);
	
	return version;
}
