//
//  jtb_butt_hook.cpp
//  butt
//
//  Created by jtb on 10/29/18.
//  Copyright © 2018 JT Bullitt. All rights reserved.
//

#include "jtb_butt_hook.hpp"
#include "jtb.hpp" // general utility functions

/*
 jtb's add-ons for integration with the Planetary Sound Machine.
 20181029
 
 */

void jtb_butt_hook :: UsageExit(int exitval) const {
    std::string usage="Usage: butt [-c CONFIGPATH | -h | -v]";
    usage += "\nLaunches an instance of Noethen's 'Broadcast Using This Tool' (with minor mods by jtb)";
    usage += "\nOptions:";
    usage += "\n   -c CONFIGPATH     Launch using the config file at CONFIGPATH";
    usage += "\n   -h, --help        print this help";
    usage += "\n   -v, --version     print only the version";
    std::cout << usage << std::endl;
    exit(exitval);
}


/*
 constructor
 Each instance of jtb_butt_hook performs a different function, depending on its args.
 
 Some instances of jtb_butt_hook require the path to butt's config file as the first arg.
 This determines the location of the psm audio program's workspace dir. With this
 knowledge, jtb_butt_hook can access any of psm's file locations.
 
 */
jtb_butt_hook :: jtb_butt_hook(std::string configPath) {
    Init (configPath);
}

// write a log message
jtb_butt_hook :: jtb_butt_hook(std::string configPath, std::string message) {
    Init (configPath);
	Log(message);
}

// examine the command line arguments and delete the old log file (if present)
jtb_butt_hook :: jtb_butt_hook(int argc, char **argv) {
	switch (argc) {
		case 2:
			// print the version number and exit
			if (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version")) {
				std::cout << "butt " << VERSION << " " << BuildVersion() << std::endl;
				exit(0);
            } else if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
                UsageExit(0);
            } else {
                UsageExit(1);
            }
			break;
			
		case 3:
			// get the config file
			if (!strcmp(argv[1], "-c")) {
				char *cfg_path = (char*)malloc((strlen(argv[2])+1) * sizeof(char));
				strcpy(cfg_path, argv[2]);
				Init(cfg_path);  // validate the paths, etc....
            } else {
                UsageExit(1);
            }
			break;
			
		default:
            UsageExit(1);
			break;
	}
	
	// delete the old logfile
	if (jtb::FileExists(_logFile)) {
		if ( remove(_logFile.c_str()) != 0 ) {
			perror( "Unable to delete old log file.");
			jtb::ByeBye(1);
		}
	}
	
	
	// from here on, butt takes care of the rest....
}



// initialize member variables
void jtb_butt_hook :: Init(std::string configPath) {
    _configPath = configPath; // Ex: /Users/jtb/earthsound/phme1/workspace/buttConfig.txt
	if (! jtb::FileExists(_configPath)) {
		std::cout << MYNAME << ": Can't find config file (" << _configPath << ")." << std::endl;
		jtb::ByeBye(1);
	}
	_workspaceDir = dirname((char*)_configPath.c_str()); // Ex: /Users/jtb/earthsound/phme1/workspace
	_programName = basename(dirname((char *)_workspaceDir.c_str())); // Ex: phme1

	_programNameUPPER="";	// Ex: PHME1
	std::locale loc;
	for (std::string::size_type i=0; i<_programName.length(); ++i)
		_programNameUPPER += std::toupper(_programName[i],loc);
	
	_logFile = _workspaceDir + "/buttLog.txt";
}



/*
 destructor
 */
jtb_butt_hook :: ~jtb_butt_hook() {
    
}

void jtb_butt_hook::PrintInfo(std::string message) {
	char msg[1024];
	strncpy(msg,message.c_str(),1024);
	print_info(msg,0);
}

void jtb_butt_hook::PrintStreamName() {
	std::string msg = "⟹ PSM program " + _programNameUPPER + " ⟸";
	PrintInfo(msg);
}


/*
 ErrorSet() -- Set an error file
 */
void jtb_butt_hook::ErrorSet(std::string type) {
    std::string errFile = ErrorFilePath(type);
    std::ofstream s_errFile;
    s_errFile.open(errFile);
    if (!s_errFile.is_open()) {
        std::cout << MYNAME << ": Can't create " << type << " error file (" << errFile << ")." << std::endl;
        jtb::ByeBye(1);
    } else {
//		std::cout << MYNAME << ": setting errFile '" << errFile << "'." << std::endl;
        s_errFile << type << std::endl; // (we can actually write anything at all to the file)
    }
    s_errFile.close();
}


/*
 ErrorClear() -- Clear an error file
 */
void jtb_butt_hook::ErrorClear(std::string type) {
    std::string errFile = ErrorFilePath(type);
    if (jtb::FileExists(errFile)) {
        if ( remove(errFile.c_str()) != 0 ) {
			std::cout << MYNAME << ": Can't delete " << type << " error file (" << errFile << ")." << std::endl;
//            perror( "Unable to delete old error file.");
            jtb::ByeBye(1);
        }
    }
//    std::cout << MYNAME << ": clearing errFile '" << errFile << "'." << std::endl;
}

// Clear all the error files
void jtb_butt_hook::ErrorClear() {
    ErrorClear("songfile");
}

/*
 ErrorFilePath -- construct a path to an error file of the given type
 error files live in the workspace dir and look like:
    buttError.songfile.txt
    buttError.foobar.txt

 */
std::string jtb_butt_hook::ErrorFilePath(std::string type) {
    if (!std::regex_match (type, std::regex("[a-zA-Z0-9_-.]+") )) {
        std::cout << MYNAME << ": Error type '" << type << "' not allowed." << std::endl;
        jtb::ByeBye(1);
    }
    return (_workspaceDir + "/buttError." + type + ".txt");
}


/*
 log() -- append a log message
 */
void jtb_butt_hook::Log(std::string msg) {
	std::ofstream s_logFile;
	s_logFile.open(_logFile, std::ios::app);
	if (!s_logFile.is_open()) {
		std::cout << MYNAME << ": Can't append to log file (" << _logFile << ")." << std::endl;
		jtb::ByeBye(1);
	} else {
		s_logFile << msg << std::endl;
	}
	s_logFile.close();
}


/*
 log() -- write a message to the log file
 */
void jtb_butt_hook::Log(char *msg) {
    Log (std::string(msg));
}


