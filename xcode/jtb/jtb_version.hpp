//
//  jtb_version.hpp
//  butt
//
//  Created by jtb on 2/21/19.
//  Copyright © 2019 JT Bullitt. All rights reserved.
//

#ifndef jtb_version_hpp
#define jtb_version_hpp
#include <stdio.h>
#include <string>
#include "config.h"  // VERSION

/*
 An attempt at building useful version numbers.
 The number is based on the MAJOR and MINOR version numbers and XCode's built-in
 build time (__DATE__ and __TIME__).
 
 For an exquisitely thoughtful discussion of version numbering,
 see "Semantic Versioning" by Tom Preston-Werner https://semver.org/
 
 In this implementation, version numbers look like this:
 
 widget 1.03.20181216.1525
 L program   |        |
 | |   |        L hour and minute
 | |   L year month day
 | L minor version (VERSION_MINOR)
 L major version (VERSION_MAJOR)
 
 */

#define VERSION_PROGRAM_NAME "+jtb"

// VERSION_MAJOR should be changed only if the build's API is NOT
// backward-compatible with previous builds.
#define VERSION_MAJOR 0

// VERSION_MINOR should be changed when adding backward-compatible features.
#define VERSION_MINOR 1

// VERSION_PATCH_FORMAT is the time stamp, and reflects backward-compatible bug fixes (patches).
#define VERSION_PATCH_FORMAT "%Y%m%d.%H%M"


std::string BuildVersion();

#endif

