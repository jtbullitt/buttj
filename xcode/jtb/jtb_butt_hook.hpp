//
//  jtb_butt_hook.hpp
//  butt
//
//  Created by jtb on 10/29/18.
//  Copyright © 2018 JT Bullitt. All rights reserved.
//

#ifndef jtb_butt_hook_hpp
#define jtb_butt_hook_hpp

#include <stdio.h>
#include <iostream>
#include <libgen.h>  // dirname()
#include <unistd.h> // getcwd()
#include "config.h"  // VERSION
#include <regex>    // regex_match
#include "fl_funcs.h" // for print_info()
#include <locale>	// std::locale, std::toupper

#include "jtb_version.hpp"

#define MYNAME "butt_hook"

#define LOGFILE_NAME "buttLog.txt"
/*
 A simple hook that gets us into butt...
 See comments in the constructor.
 */

class jtb_butt_hook {
public:
    jtb_butt_hook (std::string); // config pathname only
    jtb_butt_hook (std::string, std::string); // config pathname and a message
    jtb_butt_hook (int, char **); // command line args
    ~jtb_butt_hook ();
    
    void UsageExit(int) const;
    
    void Log(std::string); // write a message
    void Log(char *);
    void ErrorSet(std::string);
    void ErrorClear(std::string);
    void ErrorClear();
	void PrintInfo(std::string);
	void PrintStreamName();
	std::string ProgramName() {return _programName;}
	std::string ProgramNameUPPER() {return _programNameUPPER;}

private:
    std::string _configPath; // path to butt config file
    std::string _workspaceDir; // directory of the program's workspace
	std::string _logFile; // path to the logfile
	std::string _errFile; // path to the error file
	std::string _programName; // name of the PSM audio program
	std::string _programNameUPPER; // UPPERCASE name of the PSM audio program

    void Init(std::string);
    std::string ErrorFilePath(std::string);
};


#endif /* jtb_butt_hook_hpp */
