//
//  jtb.cpp
//  butt
//
//  Created by jtb on 10/29/18.
//  Copyright © 2018 JT Bullitt. All rights reserved.
//

/*
 Some handy utilities.
 */
#include "jtb.hpp"

bool jtb::FileExists(const std::string & fileName) {
    return FileExists((char*)fileName.c_str());
}
bool jtb::FileExists(char *fileName) {
    std::ifstream f;
    f.open(fileName,std::ios::in);
    if (f.is_open()) {
        f.close();
        return true;
    }
    return false;
}

void jtb::ByeBye() {
	ByeBye(0); // exit with no error
}

/*
 ByeBye() -- print a message and exit
 */
void jtb::ByeBye(int errValue) {
    std::cout << "Exiting butt." << std::endl;
    exit(errValue);
}

/*
    returns true if the give path is a directory
 */

bool jtb::path_is_dir(const std::string& path) {
    struct stat s;
    if(stat(path.c_str(),&s) == 0 ) {
        if ( s.st_mode & S_IFDIR ) { // if it's a directory
            return true;
        }
    }
    return false;
}


