# buttj

This is jtb's port of Broadcast Using This Tool v. 0.1.16 (https://sourceforge.net/projects/butt/)

With additions and tweaks for better integration with Earthsound.

Successful XCode build requires building against a bunch of static libraries, which are included in xcode/lib/. 
They include

  * libfdk-aac
  * libFLAC-static
  * libfltk
  * libmp3lame
  * libogg
  * libopus
  * libportaudio
  * libsamplerate
  * libvorbis
  * libvorbisenc

To add them to XCode, go to Project Navigator, select "butt", select TARGETS, then "butt", then "General" tab. 
Then scroll down to "Linked Frameworks and Libraries" and add (+) all the .a files in the xcode/lib/ dir.

